# 欢迎来到Gmitm文档中心

GitHub仓库地址 [https://github.com/gmitm/gmitm](https://github.com/gmitm/gmitm).

## 简介
`Gmitm` - A good man in the middle ,一个方便前后端开发者进行接口调试的工具

## 功能

* dns污染检测
* 流量劫持检测
* 网络连通性检测
* 模拟低速网络
* 请求响应保存
* 请求map，响应map，重放
* js调试、注入
* https证书状态检查

* 测试环境mock
* 公网隧道
* dns主动“污染”
* http raw
* chrome调试

* 微信浏览器清除cookie等信息
